package sbu.cs;


import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import javax.imageio.ImageIO;
import javax.swing.*;

public class Server {

    /**
     * Create a server here and wait for connection to establish,
     *  then get file name and after that get file and save it in the given directory
     *
     * @param args an string array with one element
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // below is the name of directory which you must save the file in it
        String directory = args[0];     // default: "server-database"
       //create server socket
        ServerSocket socket  = new ServerSocket(4555);
        Socket s = socket.accept();
        FileInputStream filetransfer = new FileInputStream("\\C:\\Users\\Lenovo\\Desktop\\series-7\\book.pdf");
        byte b[] = new byte[1024];
        filetransfer.read(b,0,b.length);
        OutputStream o = s.getOutputStream();
        o.write(b,0,b.length);
        //send image
        JFrame image = new JFrame("server");
        image.setSize(500,500);
        image.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JLabel label = new JLabel("waiting...");
        image.add(label , BorderLayout.SOUTH);
        image.setVisible(true);
        InputStream input = s.getInputStream();
        BufferedInputStream bufferedInputStream = new BufferedInputStream(input);
        BufferedImage bufferedImage = ImageIO.read(bufferedInputStream);
        bufferedInputStream.close();
        s.close();
        JLabel label2 = new JLabel(new ImageIcon(bufferedImage));
        label.setText("done");
        image.add(label2 , BorderLayout.CENTER);

        }
    }

