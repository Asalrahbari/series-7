package sbu.cs;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Scanner;
import java.net.Socket;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

public class Client {

    /**
     * Create a socket and connect to server then send fileName and after that send file to be saved on server side.
     *  in here filePath == fileName
     *
     * @param args a string array with one element
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        String filePath = args[0];      // "sbu.png" or "book.pdf"
        //create Scanner
        Scanner scan = new Scanner(System.in);
        String fileaddress = "\\C:\\Users\\Lenovo\\Desktop\\series-7\\book.pdf";
        //create socket
        Socket s = new Socket("localhost" , 4555);
        //get bytes
        byte []b = new byte[1024];
        InputStream i = s.getInputStream();
        FileOutputStream filetransform = new FileOutputStream(fileaddress);
        i.read(b,0,b.length);
        filetransform.write(b,0,b.length);
        //send image
        JFrame image = new JFrame("client");
        image.setSize(500,500);
        image.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ImageIcon imageicon = new ImageIcon("\\C:\\Users\\Lenovo\\Desktop\\series-7\\sbu.png");
        JLabel pic = new JLabel(imageicon);
        image.add(pic);
        JButton button = new JButton("send image");
        image.add(button);
        image.setVisible(true);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                try {
                    OutputStream outstream = s.getOutputStream();
                    BufferedOutputStream output = new BufferedOutputStream(outstream);
                    Image chosenimage = imageicon.getImage();
                    BufferedImage bufferedImage = new BufferedImage(chosenimage.getWidth(null) , chosenimage.getHeight(null) , BufferedImage.TYPE_INT_ARGB);
                    Graphics graphics = bufferedImage.createGraphics();
                    graphics.drawImage(chosenimage , 0 , 0 , null);
                    graphics.dispose();
                    ImageIO.write(bufferedImage , "png" , output);
                    output.close();
                    s.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });


    }
}
